import { createRouter, createWebHistory } from 'vue-router'
import StudentList from '@/components/StudentList.vue'
import StudentDetails from '@/components/StudentDetails.vue'
import AddStudent from '@/components/AddStudent.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/studentlist',
      name: 'StudentList',
      component: StudentList
    },
    {
      path: '/students/:id',
      name: 'StudentDetails',
      component: StudentDetails
    },
    {
      path: '/add',
      name: 'AddStudent',
      component: AddStudent
    }

  ]
})

export default router
