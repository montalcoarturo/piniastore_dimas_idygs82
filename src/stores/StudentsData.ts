import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { Ref } from 'vue'
import type IStudent from '@/interfaces/IStudent'

const url = 'https://65e8dab54bb72f0a9c508303.mockapi.io/dev/apa'

export const useStudentsStore = defineStore('students', () => {
  const students = ref([] as IStudent[])
  const student: Ref<IStudent | null> = ref(null)

  function setStudents(data: IStudent[]) {
    students.value = data
    return students.value
  }
  function setStudent(data: IStudent) {
    student.value = data
  }
  return {
    getStudents,
    setStudents,
    getStudent,
    setStudent,
    createStudent,
    deleteStudent,
    students,
    student
  }
  async function getStudents() {
    try {
      const json = await fetch(url + '/Alumnos', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const response = await json.json()
      students.value = response
      return response
    } catch (error) {
      console.log(error)
    }
  }

  async function getStudent(id: string) {
    try {
      const json = await fetch(url + '/Alumnos/' + id, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const response = await json.json()
      setStudent(response)
      return response
    } catch (error) {
      console.log(error)
    }
  }

  async function createStudent(data: any) {
    try {
      const json = await fetch(url + '/Alumnos', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      const response = await json.json()
      return response
    } catch (error) {
      console.log(error)
    }
  }

  async function deleteStudent(id: string) {
    try {
      const json = await fetch(url + '/Alumnos/' + id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const response = await json.json()
      getStudents()
      return response
    } catch (error) {
      console.log(error)
    }
  }
})
