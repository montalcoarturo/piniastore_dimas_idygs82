export default interface IStudent {
    id: string
    name: string
    email: string
    group: string
}